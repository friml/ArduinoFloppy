# Floppy Arduino Shield
This shield lets you connect IDE floppy drives to your arduino, providing pullup resistors of right size on input signals. It also has connectors for SD shield and audio output.
![shieldPicture](images/Shield.png)

## Schematics
Here is schematic of shield. Nothing too complicated. Just header connected to pins. R0, R1, R2, R3 and R4 are pullups.

**Beware R5 and C1**. Theese are for reading trackwise written identificator ("filesystem" that I invented and use), so for you they are probably useless unless you want to write and read those identificators.

If you want normal output, use 0R as R5 and no **not** solder C1.

![Schematic.pdf](images/Schematic.png)

## PCB printing
If you are going to make the PCB by laser printer, [theese files](images/ToPrint.png) are what you are looking for. Just print them with 600dpi.
![images/ToPring.png](images/ToPrint.png)
>   This is older version, that misses R0 (pullup on DiskChange/Ready signal).

## Soldering
Theese pictures should help you finding components.
![images/Front.png](images/Front.png)
![images/Back.png](images/Back.png)

The SD-Card component is arduino microSD component like this (with 3V3 stabilizator)
![images/SD_front.jpg](images/SD_front.jpg)

## Finished shield
Finished shield could look something like this.
![images/finished_front.jpg](images/finished_front.jpg)
![images/finished_back.jpg](images/finished_back.jpg)
>   This is older version of PCB, which had some issues that are now solved. Thus please don't mind those cables and wierd resistor placement.
