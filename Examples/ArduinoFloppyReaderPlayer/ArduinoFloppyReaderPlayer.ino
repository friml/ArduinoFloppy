#include "SD.h"
#define SD_ChipSelectPin 9
#include "TMRpcm.h"
#include "SPI.h"

#define IN  LOW;
#define OUT  HIGH;
#define pulseDelayTime  5

#define ZERO 63
#define ONE 18

/***********************************************************************/
/*                              Pins                                   */
/***********************************************************************/
#define FLOPPY_INDEX_PIN 8           //8 on the drive INDEX

                                     //Pin ground pin 12 on floppy

#define FLOPPY_MOTOR_ENABLE_B_PIN 7  //16 on the drive. MOTOR ENABLE B
#define FLOPPY_DIR_PIN 5             //18 on the drive. DIRECTION
#define FLOPPY_STEP_PIN 4            //20 on the drive. STEP
#define FLOPPY_WRITE_DATA_PIN 3      //22 on the drive WDATE
#define FLOPPY_WRITE_ENABLE_PIN 2    //24 on the drive WGATE
#define FLOPPY_TRACK_0_PIN A4        //26 on the drive. TRACK 0

#define FLOPPY_DATA_PIN A2           //30 on the drive. RDATA

#define FLOPPY_WRITE_PROTECT_PIN A3  //34 on the drive

#define FLOPPY_DRIVE_SEL_PIN 6
#define FLOPPY_DISK_CHANGE_READY_PIN A0
#define FLOPPY_HEAD_SELECT_PIN A1
/***********************************************************************/
byte data[10] = {0};
unsigned int index = 0;
unsigned int pos = 0;
TMRpcm tmrpcm;

void setup() {
  noInterrupts(); 
  TCCR2A = bit(COM2A1) | bit(COM2B1) | bit(WGM21) | bit(WGM20);
  TCCR2B = bit(WGM22) | bit(CS20);
  OCR2A = LOW;
  OCR2B = 1;
  interrupts();

  Serial.begin(9600);
  Serial.println("Inicialization");

  //setup pins.
  pinMode(FLOPPY_DIR_PIN, OUTPUT);
  pinMode(FLOPPY_STEP_PIN, OUTPUT);
  pinMode(FLOPPY_MOTOR_ENABLE_B_PIN, OUTPUT);
  pinMode(FLOPPY_INDEX_PIN, INPUT);
  pinMode(FLOPPY_TRACK_0_PIN, INPUT);
  pinMode(FLOPPY_DATA_PIN, INPUT);
  pinMode(FLOPPY_WRITE_DATA_PIN, OUTPUT);
  pinMode(FLOPPY_WRITE_ENABLE_PIN, OUTPUT);
  pinMode(FLOPPY_WRITE_PROTECT_PIN, INPUT);
  pinMode(FLOPPY_DRIVE_SEL_PIN, OUTPUT);
  pinMode(FLOPPY_DISK_CHANGE_READY_PIN, INPUT);
  pinMode(FLOPPY_HEAD_SELECT_PIN, OUTPUT);

  if (!SD.begin(SD_ChipSelectPin)) {
  Serial.println("SD fail");
  return;
  }
  tmrpcm.speakerPin = 10;
  tmrpcm.setVolume(5);
  //turn the motor off initially
  digitalWrite(10, LOW);
  digitalWrite(FLOPPY_DRIVE_SEL_PIN, LOW);
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, HIGH);
  //writeData(0b10101010);
  stepAllTheWayOut();
  stepIn();
  Serial.println("Ready");
}
void stepOut() {
  digitalWrite(FLOPPY_DIR_PIN,HIGH);
  stepPulse();
}

void stepIn() {
  digitalWrite(FLOPPY_DIR_PIN,LOW);
  stepPulse();
}

void stepAllTheWayIn() {
  for(int i=0;i<81;i++) {
    stepIn();
  }
}

void stepAllTheWayOut() {
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, HIGH);
  while (digitalRead(FLOPPY_TRACK_0_PIN) == 1){
    stepOut();
  }
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, LOW);
}

void writeData(word data){
  Serial.println("Writing");
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, LOW);
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, LOW);
  delay(700);
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, HIGH);
  stepIn();
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, LOW);
  delay(700);

  for(unsigned index = 0; index<16; index++){
    for(unsigned i = 0; i<3; i++){
      if (((data >> index) & 1) == 1)
        OCR2A = ONE;
      else
        OCR2A = ZERO;
    delay(700);
    digitalWrite(FLOPPY_WRITE_ENABLE_PIN, HIGH);
    stepIn();
    digitalWrite(FLOPPY_WRITE_ENABLE_PIN, LOW);
    }
  }
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, HIGH);
}

word readData(){
  word data = 0;
  byte nibble = 0;
  Serial.println("Reading");
  stepAllTheWayOut();
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, LOW);
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, HIGH);
  stepAllTheWayOut();
  delay(500);
  stepIn();

  for(unsigned index = 0; index<16; index++){
    for(unsigned i = 0; i<3; i++){
      delay(50);
      if (analogRead(FLOPPY_DATA_PIN) >= 600)
        nibble ^= (1 << i);
      stepIn();
    }
    if((((nibble >> 0) & 1) && ((nibble >> 1) & 1)) || (((nibble >> 1) & 1) && ((nibble >> 2) & 1)) || (((nibble >> 2) & 1) && ((nibble >> 0) & 1)))
      data ^= (1 << index);
    nibble = 0;
  }
  return data; 
}

void stepPulse() {
  digitalWrite(FLOPPY_STEP_PIN,LOW);
  delay(pulseDelayTime);
  digitalWrite(FLOPPY_STEP_PIN,HIGH);
}

void loop() {
  while(digitalRead(FLOPPY_DISK_CHANGE_READY_PIN) != 1){
    delay(100);
    stepOut();
  }
  stepAllTheWayOut();
  switch (readData()){
    case 1:
      tmrpcm.play("1.wav");
      break;
    case 2:
      tmrpcm.play("2.wav");
      break;
    case 3:
      tmrpcm.play("3.wav");
      break;
    case 4:
      tmrpcm.play("4.wav");
      break;
    case 5:
      tmrpcm.play("5.wav");
      break;
    case 6:
      tmrpcm.play("6.wav");
      break;
    case 7:
      tmrpcm.play("7.wav");
      break;
    case 8:
      tmrpcm.play("8.wav");
      break;
    case 9:
      tmrpcm.play("9.wav");
      break;
    case 10:
      tmrpcm.play("10.wav");
      break;
    case 11:
      tmrpcm.play("11.wav");
      break;
    case 12:
      tmrpcm.play("12.wav");
      break;
    case 13:
      tmrpcm.play("13.wav");
      break;
    case 14:
      tmrpcm.play("14.wav");
      break;
    case 15:
      tmrpcm.play("15.wav");
      break;
    case 16:
      tmrpcm.play("16.wav");
      break;
    case 17:
      tmrpcm.play("17.wav");
      break;
    case 18:
      tmrpcm.play("18.wav");
      break;
    case 19:
      tmrpcm.play("19.wav");
      break;
    case 20:
      tmrpcm.play("20.wav");
      break;
    case 21:
      tmrpcm.play("21.wav");
      break;
    case 22:
      tmrpcm.play("22.wav");
      break;
    case 23:
      tmrpcm.play("23.wav");
      break;
    case 24:
      tmrpcm.play("24.wav");
      break;
    case 25:
      tmrpcm.play("25.wav");
      break;
    case 26:
      tmrpcm.play("26.wav");
      break;
    case 27:
      tmrpcm.play("27.wav");
      break;
    case 28:
      tmrpcm.play("28.wav");
      break;
    case 29:
      tmrpcm.play("29.wav");
      break;
    case 30:
      tmrpcm.play("30.wav");
      break;
    case 31:
      tmrpcm.play("31.wav");
      break;
    case 32:
      tmrpcm.play("32.wav");
      break;
    case 33:
      tmrpcm.play("33.wav");
      break;
    case 34:
      tmrpcm.play("34.wav");
      break;
    case 35:
      tmrpcm.play("35.wav");
      break;
    case 36:
      tmrpcm.play("36.wav");
      break;
    case 37:
      tmrpcm.play("37.wav");
      break;
    case 38:
      tmrpcm.play("38.wav");
      break;
    case 39:
      tmrpcm.play("39.wav");
      break;
    case 40:
      tmrpcm.play("40.wav");
      break;
    case 41:
      tmrpcm.play("41.wav");
      break;
    case 42:
      tmrpcm.play("42.wav");
      break;
    case 43:
      tmrpcm.play("43.wav");
      break;
    case 44:
      tmrpcm.play("44.wav");
      break;
    case 45:
      tmrpcm.play("45.wav");
      break;
    case 46:
      tmrpcm.play("46.wav");
      break;
    case 47:
      tmrpcm.play("47.wav");
      break;
    case 48:
      tmrpcm.play("48.wav");
      break;
    case 49:
      tmrpcm.play("49.wav");
      break;
    case 50:
      tmrpcm.play("50.wav");
    default:
      tmrpcm.play("err.wav");
      break;
  }
  //tmrpcm.play("music.wav");
  Serial.println("*******************************");
  Serial.println("You can remove floppy from drive");
  stepAllTheWayOut();
  while(digitalRead(FLOPPY_DISK_CHANGE_READY_PIN) == 1){
    delay(5000);
    stepOut();
  }
  digitalWrite(10, LOW);
}

