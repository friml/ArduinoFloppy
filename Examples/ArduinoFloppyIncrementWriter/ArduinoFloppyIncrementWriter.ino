#define IN  LOW;
#define OUT  HIGH;
#define pulseDelayTime  5

#define ZERO 63
#define ONE 18

/***********************************************************************/
/*                              Pins                                   */
/***********************************************************************/
#define FLOPPY_INDEX_PIN 8           //8 on the drive INDEX

                                     //Pin ground pin 12 on floppy

#define FLOPPY_MOTOR_ENABLE_B_PIN 7  //16 on the drive. MOTOR ENABLE B
#define FLOPPY_DIR_PIN 5             //18 on the drive. DIRECTION
#define FLOPPY_STEP_PIN 4            //20 on the drive. STEP
#define FLOPPY_WRITE_DATA_PIN 3      //22 on the drive WDATE
#define FLOPPY_WRITE_ENABLE_PIN 2    //24 on the drive WGATE
#define FLOPPY_TRACK_0_PIN A4        //26 on the drive. TRACK 0

#define FLOPPY_DATA_PIN A2           //30 on the drive. RDATA

#define FLOPPY_WRITE_PROTECT_PIN A3  //34 on the drive

#define FLOPPY_DRIVE_SEL_PIN 6
#define FLOPPY_DISK_CHANGE_READY_PIN A0
#define FLOPPY_HEAD_SELECT_PIN A1
/***********************************************************************/

byte data[10] = {0};
unsigned int index = 0;
unsigned int pos = 0;
unsigned number=0;

void setup() {
  noInterrupts(); 
  TCCR2A = bit(COM2A1) | bit(COM2B1) | bit(WGM21) | bit(WGM20);
  TCCR2B = bit(WGM22) | bit(CS20);
  OCR2A = LOW;
  OCR2B = 1;
  interrupts();

  Serial.begin(9600);
  Serial.println("Inicialization");

  //setup pins.
  pinMode(FLOPPY_DIR_PIN, OUTPUT);
  pinMode(FLOPPY_STEP_PIN, OUTPUT);
  pinMode(FLOPPY_MOTOR_ENABLE_B_PIN, OUTPUT);
  pinMode(FLOPPY_INDEX_PIN, INPUT);
  pinMode(FLOPPY_TRACK_0_PIN, INPUT);
  pinMode(FLOPPY_DATA_PIN, INPUT);
  pinMode(FLOPPY_WRITE_DATA_PIN, OUTPUT);
  pinMode(FLOPPY_WRITE_ENABLE_PIN, OUTPUT);
  pinMode(FLOPPY_WRITE_PROTECT_PIN, INPUT);
  pinMode(FLOPPY_DRIVE_SEL_PIN, OUTPUT);
  pinMode(FLOPPY_DISK_CHANGE_READY_PIN, INPUT);
  pinMode(FLOPPY_HEAD_SELECT_PIN, OUTPUT);

  
  //turn the motor off initially
  digitalWrite(FLOPPY_DRIVE_SEL_PIN, LOW);
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, HIGH);
  //writeData(0b10101010);
  Serial.println("Ready");
}
void stepOut() {
  digitalWrite(FLOPPY_DIR_PIN,HIGH);
  stepPulse();
}

void stepIn() {
  digitalWrite(FLOPPY_DIR_PIN,LOW);
  stepPulse();
}

void stepAllTheWayIn() {
  for(int i=0;i<81;i++) {
    stepIn();
  }
}

void stepAllTheWayOut() {
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, HIGH);
  while (digitalRead(FLOPPY_TRACK_0_PIN) == 1){
    stepOut();
  }
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, LOW);
}

void writeData(word data){
  Serial.println("Writing");
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, LOW);
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, LOW);
  delay(700);
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, HIGH);
  stepIn();
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, LOW);
  delay(700);

  for(unsigned index = 0; index<16; index++){
    for(unsigned i = 0; i<3; i++){
      if (((data >> index) & 1) == 1)
        OCR2A = ONE;
      else
        OCR2A = ZERO;
    delay(700);
    digitalWrite(FLOPPY_WRITE_ENABLE_PIN, HIGH);
    stepIn();
    digitalWrite(FLOPPY_WRITE_ENABLE_PIN, LOW);
    }
  }
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, HIGH);
}

word readData(){
  word data = 0;
  byte nibble = 0;
  Serial.println("Reading");
  stepAllTheWayOut();
  digitalWrite(FLOPPY_MOTOR_ENABLE_B_PIN, LOW);
  digitalWrite(FLOPPY_WRITE_ENABLE_PIN, HIGH);
  stepAllTheWayOut();
  delay(500);
  stepIn();

  for(unsigned index = 0; index<16; index++){
    for(unsigned i = 0; i<3; i++){
      delay(50);
      if (analogRead(FLOPPY_DATA_PIN) >= 600)
        nibble ^= (1 << i);
      stepIn();
    }
    if((((nibble >> 0) & 1) && ((nibble >> 1) & 1)) || (((nibble >> 1) & 1) && ((nibble >> 2) & 1)) || (((nibble >> 2) & 1) && ((nibble >> 0) & 1)))
      data ^= (1 << index);
    nibble = 0;
  }
  return data; 
}

void stepPulse() {
  digitalWrite(FLOPPY_STEP_PIN,LOW);
  delay(pulseDelayTime);
  digitalWrite(FLOPPY_STEP_PIN,HIGH);
}

void loop() {
  while(digitalRead(FLOPPY_DISK_CHANGE_READY_PIN) != 1){
    delay(1000);
    stepOut();
  }
  stepAllTheWayOut();
  Serial.println("*******************************");
  Serial.print("Writing on floppy: ");
  Serial.println(number);
  writeData(number);
  Serial.println(readData());
  number++;
  Serial.println("*******************************");
  Serial.println("You can remove floppy from drive");
  stepAllTheWayOut();
  while(digitalRead(FLOPPY_DISK_CHANGE_READY_PIN) == 1){
    delay(5000);
    stepOut();
  }
}

