# ArduinoFloppy
Repository that should contain informations about connecting floppy drives to arduino. 

## Todo:
- Library
    - [ ] To control signals (head movements, motors,..)
    - [ ] To write identificator
    - [ ] To read identificator
    - [ ] To read FAT12
    - [ ] To write FAT12

- Shield
    - [x] Ready to print PNG
    - [ ] Gerber data
    - [x] Schematic
    - [x] Photos
    - [x] Altium project


